# Hello ATtiny1614 Blink

Hello world example for the [ATtiny1614](http://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-DataSheet-DS40001995B.pdf) for blinking. Based on [Fab Academy blink example](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo) by Neil Gershenfeld.



## Programming

[Install MegaTinyCore](https://www.electronics-lab.com/project/getting-started-with-the-new-attiny-chips-programming-the-microchips-0-series-and-1-series-attiny-with-the-arduino-ide/) addon for your Arduino IDE.

[Get pyupdi](https://github.com/mraardvark/pyupdi). Add it to the PATH (if you want to). Use it with a modified command from below. 

- `-d` specifies the board you want to use
- `-c` specifies the programmer (FTDI connection) device
- `-b` specifies the baud rate
- `-v` tells it to be verbose
- `-f` specifies the input program to be used

```
pyupdi.py -d tiny1614 -c /dev/tty.usbserial-FTBBWUTD -b 57600 -v -f ~/Documents/Arduino/build/412blink.ino.hex
```

## Reference Images

![Hello being programmed with the FTDI to UPDI adapter](hello-adapter.jpg)

![ATtinyX14 pinout](ATtiny_x14_pinout.png)

