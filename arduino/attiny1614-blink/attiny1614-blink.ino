//
// hello.t412.blink.ino
//
// tiny412 blink hello-world
//
// Neil Gershenfeld 12/8/19
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

#include <avr/io.h>

#define LED_AVR PIN1_bm

void setup() {
   PORTB.DIRSET = LED_AVR;
   //pinMode(LED_ARDUINO,OUTPUT); // does the same thing
   }

void loop() {
   //
   // port read-modify-write: 304 ns/bit
   //    6 cycles at 20 MHz
   //
   PORTB.OUT |= LED_AVR;
   delay(1000);
   PORTB.OUT &= ~LED_AVR;
   delay(250);
   //
   // port bit write: 96 ns
   //    2 cycles at 20 MHz
   //
   PORTB.OUTSET = LED_AVR;
   PORTB.OUTCLR = LED_AVR;
   //
   // virtual port write: 50 ns
   //    1 cycle at 20 MHz
   //
   VPORTB.OUT |= LED_AVR;
   VPORTB.OUT &= ~LED_AVR;
   }